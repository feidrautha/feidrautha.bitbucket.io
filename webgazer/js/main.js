window.onload = function () {

    //start the webgazer tracker
    webgazer.setRegression('ridge')
    /* currently must set regression and tracker */
        .setTracker('clmtrackr')
        .setGazeListener(function (data, clock) {
            detectElement(data);
            /* data is an object containing an x and y key which are the x and y prediction coordinates (no bounds limiting) */
            //   console.log(clock); /* elapsed time in milliseconds since webgazer.begin() was called */
        })
        .begin()
        .showPredictionPoints(true);
    /* shows a square every 100 milliseconds where current prediction is */
    //Set up the webgazer video feedback.
    var setup = function () {

        //Set up the main canvas. The main canvas is used to calibrate the webgazer.
        var canvas = document.getElementById("plotting_canvas");
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
        canvas.style.position = 'fixed';
        document.getElementById("regression").innerHTML = "<p>" + " ridge" + "</p>";


        var style = document.createElement('style');
        style.innerHTML =
            '.active {' +
            'background-color: yellow!important;' +
            '  transition: background-color 2s ease;!important;' +
            '}' +
            '* {' +
            '-webkit-transition: all 2s ease;' +
            '-moz-transition:    all 2s ease;' +
            '-ms-transition:     all 2s ease;' +
            '-o-transition:      all 2s ease;' +
            '}';
        var ref = document.querySelector('script');

        ref.parentNode.insertBefore(style, ref);
    };


    function checkIfReady() {
        if (webgazer.isReady()) {
            setup();
        } else {
            setTimeout(checkIfReady, 100);
        }
    }

    setTimeout(checkIfReady, 100);
};

window.onbeforeunload = function () {
    webgazer.end(); //Uncomment if you want to save the data even if you reload the page.
    // window.localStorage.clear(); //Comment out if you want to save data across different sessions
};

/**
 * Restart the calibration process by clearing the local storage and reseting the calibration point
 */
function Recalibrate(regressionType) {
    document.getElementById("Accuracy").innerHTML = "<a>Not yet Calibrated</a>";
    ClearCalibration();
    PopUpInstruction();
    (regressionType === undefined) ? document.getElementById("regression").innerHTML = "<p>" + " ridge" + "</p>" :
        document.getElementById("regression").innerHTML = "<p>" + " " + regressionType + "</p>";
}

function Restart() {
    document.getElementById("Accuracy").innerHTML = "<a>Not yet Calibrated</a>";
    ClearCalibration();
    PopUpInstruction();
}

function Calibrate() {
    PopUpInstruction();
}


function detectElement(data) {
    if (data) {
        var element = document.elementFromPoint(data.x, data.y);
        $(element).addClass('active');
        $("*").not(element).removeClass('active');
    }
}

function changeRegression(regression) {
    Recalibrate(regression);

    webgazer.setRegression(regression)
        .setTracker('clmtrackr')
        .setGazeListener(function (data, clock) {
            detectElement(data)
            //   console.log(clock); /* elapsed time in milliseconds since webgazer.begin() was called */
        })
        .begin()
        .showPredictionPoints(true);
    alert(regression);
}